" Lox syntax file
" Language: Lox
" Maintainer: Tarek Nawara 

:if exists("b:current_syntax")
:  finish
:endif

" keywords
:syntax keyword loxKeyword class fun var fn and or
:syntax keyword loxKeyword for while return this super

" booleans
:syntax keyword loxBoolean true false

" constants
:syntax keyword loxConstant nil

" functions
:syntax keyword loxFunction print clock stringify len append
:syntax keyword loxFunction del slice read number sort split

" operators
:syntax match loxOperator "\v\*"
:syntax match loxOperator "\v\+"
:syntax match loxOperator "\v\-"
:syntax match loxOperator "\v/"
:syntax match loxOperator "\v\="
:syntax match loxOperator "\v!"
:syntax match loxOperator "\v#\{"

" conditionals
:syntax keyword loxConditional if else and or else

" numbers
:syntax match loxNumber "\v\-?\d*(\.\d+)?"

" strings
:syntax region loxString start="\v\"" end="\v\""

" comments
:syntax match loxComment "\v//.*$"

:highlight link loxKeyword Keyword
:highlight link loxBoolean Boolean
:highlight link loxConstant Constant
:highlight link loxFunction Function
:highlight link loxOperator Operator
:highlight link loxConditional Conditional
:highlight link loxNumber Number
:highlight link loxString String
:highlight link loxComment Comment

:let b:current_syntax = "lox"
