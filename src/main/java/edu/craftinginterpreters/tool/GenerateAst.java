package edu.craftinginterpreters.tool;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenerateAst {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Usage: generate_ast <output directory>");
            System.exit(64);
        }
        var outputDir = args[0];
        var expressions = new HashMap<String, List<String>>();
        var statements = new HashMap<String, List<String>>();

        // Expressions.
        expressions.put("Assign",      Arrays.asList("Token name", "Expr value"));
        expressions.put("Binary",      Arrays.asList("Expr left", "Token operator", "Expr right"));
        expressions.put("Call",        Arrays.asList("Expr callee", "Token paren", "List<Expr> arguments"));
        expressions.put("Index",       Arrays.asList("Expr container", "Token bracket", "Expr index"));
        expressions.put("Fn",          Arrays.asList("List<Token> params", "List<Stmt> body"));
        expressions.put("ListLiteral", Arrays.asList("List<Expr> elements"));
        expressions.put("MapLiteral",  Arrays.asList("Map<Expr,Expr> elements"));
        expressions.put("Get",         Arrays.asList("Expr object", "Token name"));
        expressions.put("Grouping",    Arrays.asList("Expr expression"));
        expressions.put("Literal",     Arrays.asList("Object value"));
        expressions.put("Logical",     Arrays.asList("Expr left", "Token operator", "Expr right"));
        expressions.put("Set",         Arrays.asList("Expr object", "Token name", "Expr value"));
        expressions.put("SetIndex",    Arrays.asList("Expr container", "Token bracket", "Expr index", "Expr value"));
        expressions.put("Super",       Arrays.asList("Token keyword", "Token method"));
        expressions.put("This",        Arrays.asList("Token keyword"));
        expressions.put("Variable",    Arrays.asList("Token name"));
        expressions.put("Unary",       Arrays.asList("Token operator", "Expr right"));

        // Statements.
        statements.put("Block",        Arrays.asList("List<Stmt> statements"));
        statements.put("Class",        Arrays.asList("Token name", "Expr.Variable superclass", "List<Stmt.Function> methods"));
        statements.put("Expression",   Arrays.asList("Expr expression"));
        statements.put("Function",     Arrays.asList("Token name", "List<Token> params", "List<Stmt> body"));
        statements.put("If",           Arrays.asList("Expr condition", "Stmt thenBranch", "Stmt elseBranch"));
        statements.put("Print",        Arrays.asList("Token end", "Expr expression"));
        statements.put("Return",       Arrays.asList("Token keyword", "Expr value"));
        statements.put("While",        Arrays.asList("Expr condition", "Stmt body"));
        statements.put("Var",          Arrays.asList("Token name", "Expr initializer"));

        defineAst(outputDir, "Expr", expressions);
        defineAst(outputDir, "Stmt", statements);
    }

    private static void defineAst(String outputDir,
                                  String baseName,
                                  Map<String, List<String>> types) throws IOException {
        var path = outputDir + "/" + baseName + ".java";
        try (var writer = new PrintWriter(path, StandardCharsets.UTF_8)) {
            writer.println("package edu.craftinginterpreters.lox;");
            writer.println();
            writer.println("import java.util.List;");
            writer.println("import java.util.Map;");
            writer.println();
            writer.println("abstract class " + baseName + "{ ");
            defineVisitor(writer, baseName, types);

            // The AST classes.
            for (var entry : types.entrySet()) {
                var className = entry.getKey();
                var fields = entry.getValue();
                defineType(writer, baseName, className, fields);
            }

            // The base accept() method.
            writer.println();
            writer.println("    abstract <R> R accept(Visitor<R> visitor);");
            writer.println("}");
        }
    }

    private static void defineVisitor(PrintWriter writer, String baseName, Map<String, List<String>> types) {
        writer.println("interface Visitor<R> {");
        for (var typeName : types.keySet()) {
            writer.printf("R visit%s%s(%s %s);\n", typeName, baseName, typeName, baseName.toLowerCase());
        }
        writer.println("}");
    }

    private static void defineType(PrintWriter writer, String baseName, String className, List<String> fieldList) {
        writer.printf("static class %s extends %s {\n", className, baseName);

        // Constructor.
        var fieldString = String.join(",", fieldList);
        writer.printf("%s (%s) {\n", className, fieldString);

        for (var field : fieldList) {
            var name = field.split(" ")[1];
            writer.printf("this.%s = %s;\n", name, name);
        }
        writer.println("}");

        // Visitor pattern.
        writer.println();
        writer.println("@Override");
        writer.println("<R> R accept(Visitor<R> visitor) {");
        writer.printf("return visitor.visit%s%s(this);\n", className, baseName);
        writer.println("}");

        // Fields.
        for (var field : fieldList) {
            writer.printf("final %s;\n", field);
        }
        writer.println("}");
    }
}
