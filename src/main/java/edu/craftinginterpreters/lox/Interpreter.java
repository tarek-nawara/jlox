package edu.craftinginterpreters.lox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Interpreter implements Expr.Visitor<Object>, Stmt.Visitor<Void> {
    private final Environment globals = new Environment();
    private final Map<Expr, Integer> locals = new HashMap<>();
    private final InputReader inputReader = new InputReader(System.in);
    private Environment environment = globals;

    public Interpreter() {
        globals.define("clock", new LoxCallable() {
            @Override
            public int arity() {
                return 0;
            }

            @Override
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                return (double) System.currentTimeMillis() / 1000.0;
            }

            @Override
            public String toString() {
                return "<native fn>";
            }
        });

        globals.define("stringify", new LoxCallable() {
            @Override
            public int arity() {
                return 1;
            }

            @Override
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                return stringify(paren, arguments.get(0));
            }

            @Override
            public String toString() {
                return "<native fn>";
            }
        });

        globals.define("len", new LoxCallable() {
            @Override
            public int arity() {
                return 1;
            }

            @Override
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                var argument = arguments.get(0);
                if (argument instanceof List list) {
                    return (double) list.size();
                } else if (argument instanceof Map map) {
                    return (double) map.size();
                } else {
                    throw new RuntimeError(paren, "Argument doesn't support len operation.");
                }
            }
        });

        globals.define("append", new LoxCallable() {
            @Override
            public int arity() {
                return 2;
            }

            @Override
            @SuppressWarnings("unchecked")
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                var container = arguments.get(0);
                if (container instanceof List list) {
                    list.add(arguments.get(1));
                } else {
                    throw new RuntimeError(paren, "Append not supported on the given argument");
                }

                return null;
            }
        });

        globals.define("del", new LoxCallable() {
            @Override
            public int arity() {
                return 2;
            }

            @Override
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                var container = arguments.get(0);
                if (container instanceof List list) {
                    list.remove(arguments.get(1));
                } else if (container instanceof Map map) {
                    map.remove(arguments.get(1));
                } else {
                    throw new RuntimeError(paren, "Append not supported on the given argument");
                }

                return null;
            }
        });

        globals.define("slice", new LoxCallable() {
            @Override
            public int arity() {
                return 3;
            }

            @Override
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                var container = arguments.get(0);
                if (container instanceof List list) {
                    list.subList(integerValue(paren, arguments.get(1)), integerValue(paren, arguments.get(2)));
                } else {
                    throw new RuntimeError(paren, "Append not supported on the given argument");
                }

                return null;
            }
        });

        globals.define("sort", new LoxCallable() {
            @Override
            public int arity() {
                return 1;
            }

            @Override
            @SuppressWarnings("unchecked")
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                var argument = arguments.get(0);
                if (argument instanceof List list) {
                    Collections.sort(list);
                } else {
                    throw new RuntimeError(paren, "Sort not supported on the given argument.");
                }
                return null;
            }
        });

        globals.define("read", new LoxCallable() {
            @Override
            public int arity() {
                return 0;
            }

            @Override
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                return inputReader.next(paren);
            }
        });

        globals.define("number", new LoxCallable() {
            @Override
            public int arity() {
                return 1;
            }

            @Override
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                var argument = arguments.get(0);
                try {
                    return Double.parseDouble(argument.toString());
                } catch (Exception e) {
                    throw new RuntimeError(paren, "Converting to number failed.");
                }
            }
        });

        globals.define("split", new LoxCallable() {
            @Override
            public int arity() {
                return 2;
            }

            @Override
            public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
                var one = arguments.get(0);
                var two = arguments.get(1);
                if (!(one instanceof String delimiter && two instanceof String s)) {
                    throw new RuntimeError(paren, "Split parameters must be string");
                }
                return new ArrayList<>(Arrays.asList(s.split(delimiter)));
            }
        });
    }

    public void interpret(List<Stmt> statements) {
        try {
            for (var statement : statements) {
                execute(statement);
            }
        } catch (RuntimeError error) {
            Lox.runtimeError(error);
        }
    }

    @Override
    public Void visitBlockStmt(Stmt.Block stmt) {
        executeBlock(stmt.statements, new Environment(environment));
        return null;
    }

    @Override
    public Void visitClassStmt(Stmt.Class stmt) {
        Object superclass = null;
        if (stmt.superclass != null) {
            superclass = evaluate(stmt.superclass);
            if (!(superclass instanceof LoxClass)) {
                throw new RuntimeError(stmt.superclass.name, "Superclass must be a class.");
            }
        }

        environment.define(stmt.name.lexeme, null);

        if (stmt.superclass != null) {
            environment = new Environment(environment);
            environment.define("super", superclass);
        }

        var methods = new HashMap<String, LoxFunction>();
        for (var method : stmt.methods) {
            var function = new LoxFunction(method, environment, method.name.lexeme.equals("init"));
            methods.put(method.name.lexeme, function);
        }

        var klass = new LoxClass(stmt.name.lexeme, (LoxClass) superclass, methods);

        if (stmt.superclass != null) {
            environment = environment.enclosing;
        }

        environment.assign(stmt.name, klass);
        return null;
    }

    @Override
    public Void visitExpressionStmt(Stmt.Expression stmt) {
        evaluate(stmt.expression);
        return null;
    }

    @Override
    public Void visitFunctionStmt(Stmt.Function stmt) {
        var function = new LoxFunction(stmt, environment, false);
        environment.define(stmt.name.lexeme, function);
        return null;
    }

    @Override
    public Void visitIfStmt(Stmt.If stmt) {
        if (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.thenBranch);
        } else if (stmt.elseBranch != null) {
            execute(stmt.elseBranch);
        }

        return null;
    }

    @Override
    public Void visitPrintStmt(Stmt.Print stmt) {
        var value = evaluate(stmt.expression);
        System.out.println(stringify(stmt.end, value));
        return null;
    }

    @Override
    public Void visitReturnStmt(Stmt.Return stmt) {
        Object value = null;
        if (stmt.value != null) value = evaluate(stmt.value);
        throw new Return(value);
    }

    @Override
    public Void visitWhileStmt(Stmt.While stmt) {
        while (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.body);
        }
        return null;
    }

    @Override
    public Void visitVarStmt(Stmt.Var stmt) {
        Object value = null;
        if (stmt.initializer != null) {
            value = evaluate(stmt.initializer);
        }

        environment.define(stmt.name.lexeme, value);
        return null;
    }

    void executeBlock(List<Stmt> statements, Environment environment) {
        var previous = this.environment;
        try {
            this.environment = environment;
            for (var statement : statements) {
                execute(statement);
            }
        } finally {
            this.environment = previous;
        }
    }

    void execute(Stmt statement) {
        statement.accept(this);
    }

    public void resolve(Expr expr, int depth) {
        locals.put(expr, depth);
    }

    @Override
    public Object visitAssignExpr(Expr.Assign expr) {
        var value = evaluate(expr.value);
        var distance = locals.get(expr);
        if (distance != null) {
            environment.assignAt(distance, expr.name, value);
        } else {
            globals.assign(expr.name, value);
        }
        return value;
    }

    @Override
    public Object visitBinaryExpr(Expr.Binary expr) {
        var left = evaluate(expr.left);
        var right = evaluate(expr.right);

        switch (expr.operator.type) {
            case GREATER:
                checkNumberOperands(expr.operator, left, right);
                return (double) left > (double) right;
            case GREATER_EQUAL:
                checkNumberOperands(expr.operator, left, right);
                return (double) left >= (double) right;
            case LESS:
                checkNumberOperands(expr.operator, left, right);
                return (double) left < (double) right;
            case LESS_EQUAL:
                checkNumberOperands(expr.operator, left, right);
                return (double) left <= (double) right;
            case BANG_EQUAL:
                return !isEqual(left, right);
            case EQUAL_EQUAL:
                return isEqual(left, right);
            case MINUS:
                checkNumberOperands(expr.operator, left, right);
                return (double) left - (double) right;
            case PLUS:
                if (left instanceof Double l && right instanceof Double r) {
                    return l + r;
                } else if (left instanceof String l && right instanceof String r) {
                    return l + r;
                }

                throw new RuntimeError(expr.operator, "Operands must be two numbers or two strings.");
            case SLASH:
                checkNumberOperands(expr.operator, left, right);
                return (double) left / (double) right;
            case STAR:
                checkNumberOperands(expr.operator, left, right);
                return (double) left * (double) right;
        }

        // Unreachable.
        return null;
    }

    @Override
    public Object visitCallExpr(Expr.Call expr) {
        var callee = evaluate(expr.callee);
        var arguments = new ArrayList<>();
        for (var argument : expr.arguments) {
            arguments.add(evaluate(argument));
        }

        if (!(callee instanceof LoxCallable function)) {
            throw new RuntimeError(expr.paren, "Can only call functions and classes.");
        }

        if (arguments.size() != function.arity()) {
            throw new RuntimeError(expr.paren,
                    String.format("Expected %d arguments but got %d.", function.arity(), arguments.size()));
        }
        return function.call(this, arguments, expr.paren);
    }

    @Override
    public Object visitIndexExpr(Expr.Index expr) {
        var container = evaluate(expr.container);
        var index = evaluate(expr.index);

        if (container instanceof List list) {
            var actualIndex = integerValue(expr.bracket, index);
            if (actualIndex < 0 || actualIndex >= list.size()) {
                throw new RuntimeError(expr.bracket, "Index is out of list bounds.");
            }

            return list.get(actualIndex);
        } else if (container instanceof Map map) {
            return map.get(index);
        } else if (container instanceof LoxInstance instance) {
            var token = Token.fromLexeme("get_index");
            var indexMethod = instance.get(token);
            if (!(indexMethod instanceof LoxFunction function)) {
                throw new RuntimeError(expr.bracket, "get_index must be a method.");
            }
            if (function.arity() != 1) {
                throw new RuntimeError(expr.bracket, "get_index method must have one argument.");
            }

            return function.call(this, List.of(index), expr.bracket);
        } else {
            throw new RuntimeError(expr.bracket, "Expression can't be indexed.");
        }
    }

    @Override
    public Object visitFnExpr(Expr.Fn expr) {
        return new LoxFn(expr, environment);
    }

    @Override
    public Object visitListLiteralExpr(Expr.ListLiteral expr) {
        var values = new ArrayList<>();
        for (var element : expr.elements) {
            values.add(evaluate(element));
        }
        return values;
    }

    @Override
    public Object visitMapLiteralExpr(Expr.MapLiteral expr) {
        var map = new HashMap<>();
        for (var entry : expr.elements.entrySet()) {
            var key = evaluate(entry.getKey());
            var value = evaluate(entry.getValue());
            map.put(key, value);
        }

        return map;
    }

    @Override
    public Object visitGetExpr(Expr.Get expr) {
        var object = evaluate(expr.object);
        if (object instanceof LoxInstance loxInstance) {
            return loxInstance.get(expr.name);
        }

        throw new RuntimeError(expr.name, "Only instances have properties.");
    }

    @Override
    public Object visitGroupingExpr(Expr.Grouping expr) {
        return evaluate(expr.expression);
    }

    @Override
    public Object visitLiteralExpr(Expr.Literal expr) {
        return expr.value;
    }

    @Override
    public Object visitLogicalExpr(Expr.Logical expr) {
        var left = evaluate(expr.left);
        if (expr.operator.type == TokenType.OR) {
            if (isTruthy(left)) return left;
        } else {
            if (!isTruthy(left)) return left;
        }

        return evaluate(expr.right);
    }

    @Override
    public Object visitSetExpr(Expr.Set expr) {
        var object = evaluate(expr.object);
        if (!(object instanceof LoxInstance)) {
            throw new RuntimeError(expr.name, "Only instances have fields.");
        }

        var value = evaluate(expr.value);
        ((LoxInstance) object).set(expr.name, value);
        return value;
    }

    @Override
    public Object visitSuperExpr(Expr.Super expr) {
        var distance = locals.get(expr);
        var superclass = (LoxClass) environment.getAt(distance, "super");
        var object = (LoxInstance) environment.getAt(distance - 1, "this");

        var method = superclass.findMethod(expr.method.lexeme);
        if (method == null) {
            throw new RuntimeError(expr.method, String.format("Undefined property '%s'.", expr.method.lexeme));
        }

        return method.bind(object);
    }

    @Override
    public Object visitThisExpr(Expr.This expr) {
        return lookUpVariable(expr.keyword, expr);
    }

    @Override
    public Object visitVariableExpr(Expr.Variable expr) {
        return lookUpVariable(expr.name, expr);
    }

    @Override
    public Object visitUnaryExpr(Expr.Unary expr) {
        var right = evaluate(expr.right);
        switch (expr.operator.type) {
            case BANG:
                return !isTruthy(right);
            case MINUS:
                checkNumberOperand(expr.operator, right);
                return -(double) right;
        }

        // Unreachable.
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object visitSetIndexExpr(Expr.SetIndex expr) {
        var container = evaluate(expr.container);
        if (container instanceof List list) {
            var index = evaluate(expr.index);
            var actualIndex = integerValue(expr.bracket, index);
            if (actualIndex < 0 || actualIndex >= list.size()) {
                throw new RuntimeError(expr.bracket, "Index out of list bounds");
            }
            var value = evaluate(expr.value);

            return list.set(actualIndex, value);
        } else if (container instanceof Map map) {
            var key = evaluate(expr.index);
            var value = evaluate(expr.value);

            return map.put(key, value);
        } else if (container instanceof LoxInstance instance) {
            var token = Token.fromLexeme("set_index");
            var method = instance.get(token);
            if (!(method instanceof LoxFunction function)) {
                throw new RuntimeError(expr.bracket, "set_index must be a method.");
            }
            if (function.arity() != 2) {
                throw new RuntimeError(expr.bracket, "set_index must have '2' argument.");
            }

            var index = evaluate(expr.index);
            var value = evaluate(expr.value);
            return function.call(this, List.of(index, value), expr.bracket);
        } else {
            throw new RuntimeError(expr.bracket, "Index is not supported on the current expression.");
        }
    }

    Object evaluate(Expr expr) {
        return expr.accept(this);
    }

    private Object lookUpVariable(Token name, Expr expr) {
        var distance = locals.get(expr);
        if (distance != null) {
            return environment.getAt(distance, name.lexeme);
        } else {
            return globals.get(name);
        }
    }

    private void checkNumberOperands(Token operator, Object left, Object right) {
        if (left instanceof Double && right instanceof Double) return;
        throw new RuntimeError(operator, "Operands must be numbers.");
    }

    private void checkNumberOperand(Token operator, Object operand) {
        if (operand instanceof Double) return;
        throw new RuntimeError(operator, "Operand must be a number.");
    }

    private int integerValue(Token token, Object index) {
        if (index instanceof Double d && d == Math.rint(d)) return d.intValue();
        throw new RuntimeError(token, "Index must be a number.");
    }

    private boolean isTruthy(Object object) {
        if (object == null) return false;
        if (object instanceof Boolean) return (boolean) object;
        return true;
    }

    private boolean isEqual(Object a, Object b) {
        if (a == null && b == null) return false;
        if (a == null) return false;

        return a.equals(b);
    }

    private String stringify(Token end, Object object) {
        if (object == null) return "nil";
        if (object instanceof Double) {
            var text = object.toString();
            if (text.endsWith(".0")) {
                text = text.substring(0, text.length() - 2);
            }
            return text;
        } else if (object instanceof LoxInstance instance) {
            var token = Token.fromLexeme("to_str");
            if (instance.has(token)) {
                var to_str = instance.get(token);
                if (to_str instanceof LoxFunction function) {
                    if (function.arity() != 0) {
                        throw new RuntimeError(end, "to_str can't have arguments");
                    }
                    var result = function.call(this, Collections.emptyList(), end);
                    if (!(result instanceof String s)) {
                        throw new RuntimeError(end, "to_str must return a string");
                    }
                    return s;
                }
            }
        }

        return object.toString();
    }
}
