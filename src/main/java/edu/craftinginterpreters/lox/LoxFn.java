package edu.craftinginterpreters.lox;

import java.util.List;

public class LoxFn implements LoxCallable {
    private final Expr.Fn declaration;
    private final Environment closure;

    public LoxFn(Expr.Fn declaration, Environment closure) {
        this.declaration = declaration;
        this.closure = closure;
    }


    @Override
    public int arity() {
        return declaration.params.size();
    }

    @Override
    public Object call(Interpreter interpreter, List<Object> arguments, Token paren) {
        var environment = new Environment(closure);
        for (int i = 0; i < declaration.params.size(); ++i) {
            environment.define(declaration.params.get(i).lexeme, arguments.get(i));
        }

        try {
            interpreter.executeBlock(declaration.body, environment);
        } catch (Return returnValue) {
            return returnValue.value;
        }
        return null;
    }

    @Override
    public String toString() {
        return "<fn anonymous>";
    }
}
