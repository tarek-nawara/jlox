package edu.craftinginterpreters.lox;

public enum ClassType {
    NONE,
    CLASS,
    SUBCLASS
}
