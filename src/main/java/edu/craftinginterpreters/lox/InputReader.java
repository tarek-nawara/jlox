package edu.craftinginterpreters.lox;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class InputReader {
    private final InputStream inputStream;
    private final BufferedReader reader;
    private StringTokenizer tokenizer;


    public InputReader(InputStream inputStream) {
        this.inputStream = inputStream;
        this.reader = new BufferedReader(new InputStreamReader(inputStream));
    }

    public String next(Token token) {
        try {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                tokenizer = new StringTokenizer(reader.readLine());
            }
        } catch (Exception e) {
            throw new RuntimeError(token, "Read input failed.");
        }

        return tokenizer.nextToken();
    }

    public double nextNumber(Token token) {
        return Double.parseDouble(next(token));
    }
}
