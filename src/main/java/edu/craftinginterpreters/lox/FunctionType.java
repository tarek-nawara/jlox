package edu.craftinginterpreters.lox;

public enum FunctionType {
    NONE,
    METHOD,
    INITIALIZER,
    FUNCTION,
    FN
}
